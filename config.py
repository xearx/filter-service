from configparser import ConfigParser

__cfg = ConfigParser()
__cfg.read('config.ini')

config = {
    'grpc': {
        'server_port': __cfg['grpc'].getint('server_port'),
        'storage_service_address': __cfg['grpc']['server_port']
    }
}
