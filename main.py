import logging
import server
from clients import storage

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    storage.connect()
    server.serve()
