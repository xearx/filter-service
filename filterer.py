def filter(iterator, iteratee: list) -> list:
    return [item for item in iteratee if iterator(item)]


def apply_filters_on_urls(filters: list, urls: list) -> list:
    for filterer in filters:
        urls = filterer(urls)
    return urls
