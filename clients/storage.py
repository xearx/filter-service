import logging
import grpc
from protos import storage_pb2, storage_pb2_grpc
from config import config


def connect():
    logging.basicConfig()
    service_address = config['grpc']['storage_service_address']
    with grpc.insecure_channel(target=service_address) as channel:
        logging.info(f'connected to storage service at {service_address}.')
        return channel
