import logging
from reppy.robots import Robots
from reppy.cache import AgentCache

agent_caches = {'*': AgentCache(agent='*', capacity=1000)}


def add_agent_cache(agent):
    if agent not in agent_caches.keys():
        agent_caches[agent] = AgentCache(agent=agent, capacity=100)


def get_or_build_agent_cache(agent):
    if agent not in agent_caches.keys():
        add_agent_cache(agent=agent)
    return agent_caches[agent]


def allowed_url(url_to_check: str, user_agent="*") -> bool:
    logging.info(f'check allowing of {url_to_check}')
    cache = get_or_build_agent_cache(agent=user_agent)
    is_allowed = cache.allowed(url_to_check)
    logging.info(f'url {url_to_check}: {is_allowed}')
    return is_allowed


def allowed_urls(urls_to_check, user_agent="*") -> list:
    return [url for url in urls_to_check if allowed_url(url)]
