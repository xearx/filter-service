from concurrent import futures
import logging
import grpc
from protos import filterer_pb2_grpc, filterer_pb2
from config import config
import robots, filterer


class FiltererServicer(filterer_pb2_grpc.FiltererServicer):
    def FilterURLs(self, request: filterer_pb2.URLs, context):
        filtered_urls = filterer.apply_filters_on_urls([robots.allowed_urls],
                                                       request.urls)
        return filterer_pb2.URLs(urls=filtered_urls)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=100))
    filterer_pb2_grpc.add_FiltererServicer_to_server(FiltererServicer(),
                                                     server)
    port = config['grpc']['server_port']
    server_address = f'[::]:{port}'
    server.add_insecure_port(server_address)
    try:
        server.start()
        logging.info(f'server started at {server_address}')
        server.wait_for_termination()
    except KeyboardInterrupt:
        logging.info('shutting server down...')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    serve()