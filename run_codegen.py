from grpc_tools import protoc

protoc.main((
    '',
    '-I..',
    '--python_out=.',
    '--grpc_python_out=.',
    'protos/filterer.proto',
    'protos/storage.proto',
))
